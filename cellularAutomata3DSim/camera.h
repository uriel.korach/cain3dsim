#pragma once
#include <QOpenGLFunctions_4_5_Core>
#include <QMatrix4x4>
enum Direction {UP,DOWN, RIGHT, LEFT};

class Camera
{
public:
	Camera(QVector3D initPos, float w, float h, float spacing_);
	~Camera();
	void rotate(int pitch, int yaw);
	void move(Direction dir, float speed);

	float spacing;
	float zoomLevel;
	bool isFPSMode = false;

	QVector3D position, initPos;
	QVector3D right, up;
	QVector3D direction;
	QMatrix4x4 rotation;
	QMatrix4x4 translation;
	QMatrix4x4 perspective;
	QMatrix4x4 lookAt;

};

