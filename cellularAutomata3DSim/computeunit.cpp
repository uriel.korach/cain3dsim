#include "computeunit.h"



ComputeUnit::ComputeUnit()
{
	first = 0;
	second = 1;
	 
}


ComputeUnit::~ComputeUnit()
{
}


void ComputeUnit::initializeCL()
{
	std::vector<cl::Platform> platforms;
	cl::Platform::get(&platforms);
	int count = 0;
	for (auto current_platform : platforms)
	{
		std::string name;
		current_platform.getInfo(CL_PLATFORM_NAME, &name);
		std::cout << "Platform name: " << name << '\n';
		std::vector<cl::Device> devices;
		current_platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
		std::cout << "The GPU devices in it:\n";
		for (auto current_device : devices)
		{
			if (CL_SUCCESS == current_device.getInfo(CL_DEVICE_NAME, &name))
				std::cout << "Device name: " << name << '\n';
			current_platform.getInfo(CL_PLATFORM_NAME, &name);
			if (name.find("NVIDIA") != std::string::npos)
			{
				platform = current_platform;
				device = current_device;
				count++;
			}

		}
	}
	std::cout << "number of supported devices:" << count << '\n';
#ifdef _WIN32
	cl_context_properties cps[] = { CL_GL_CONTEXT_KHR,
				(cl_context_properties)wglGetCurrentContext(),
				CL_WGL_HDC_KHR,
				(cl_context_properties)wglGetCurrentDC(),
				CL_CONTEXT_PLATFORM,
				(cl_context_properties)platform(),
				0
	};
	cl_int err = CL_SUCCESS;
#endif

	context = cl::Context(device, cps, NULL, NULL, &err);
	std::cout << "Context creation error number (0 is good): " << err << std::endl;

	CreateProgram("kernel.cl", "basicCA");
	queue = cl::CommandQueue(context, device);
}


void ComputeUnit::createImageFromGL(cl_GLenum target, GLuint dataID1, GLuint dataID2)
{
	cl_int err;
	ImageData1 =  new cl::ImageGL(context, CL_MEM_READ_WRITE, target, 0, dataID1, &err);
	
	if (err != CL_SUCCESS)
		std::cout << "ImageGL number 1 creation error number: " << err << std::endl;

	ImageData2 = new cl::ImageGL(context, CL_MEM_READ_WRITE, target, 0, dataID2, &err);

	if (err != CL_SUCCESS)
		std::cout << "ImageGL number 2 creation error number: " << err << std::endl;
}

void ComputeUnit::releaseImages()
{
	delete ImageData1;
	delete ImageData2;
}


void ComputeUnit::CreateProgram(const std::string & file, const std::string & function)
{
	std::ifstream File(file);
	std::string src(std::istreambuf_iterator<char>(File), (std::istreambuf_iterator<char>()));

	cl::Program::Sources sources(1, std::make_pair(src.c_str(), src.length() + 1));
	cl::Program newProgram(context, sources);

	cl_int err = newProgram.build("-cl-std=CL1.2");
	std::cout << "program build error number (0 is good): " << err << std::endl;
	if (err == CL_SUCCESS)
		program = newProgram;
	if (err == CL_BUILD_PROGRAM_FAILURE) {
		// Determine the size of the log
		size_t log_size;
		clGetProgramBuildInfo(newProgram(), device(), CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);

		// Allocate memory for the log
		char *log = (char *)malloc(log_size);

		// Get the log
		clGetProgramBuildInfo(newProgram(), device(), CL_PROGRAM_BUILD_LOG, log_size, log, NULL);

		// Print the log
	std::cout <<log <<'\n';
	}

	cl::Kernel newKernel(program, function.c_str(), &err);
	std::cout << "kernel creation error number (0 is good): " << err << std::endl;
	if (err == CL_SUCCESS)
		kernel = newKernel;

}

void ComputeUnit::releaseImageAndAll()
{
	
}

void ComputeUnit::switchData()
{
	first = first == 0 ? 1 : 0;
	second = second == 0 ? 1 : 0;

}


void ComputeUnit::compute(int width, int height, int depth)
{
	cl_int err;

	std::vector<cl::Memory> images;
	images.push_back(*ImageData1);
	images.push_back(*ImageData2);

	err = queue.enqueueAcquireGLObjects(&images);
	queue.finish();
	kernel.setArg(first, *ImageData1);
	kernel.setArg(second, *ImageData2);

	kernel.setArg(2, survive);
	kernel.setArg(3, born);
	kernel.setArg(4, isMoore ? 1 : 0);
	kernel.setArg(5, numOfStates);
	err = queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(width, height, depth));

	err = queue.enqueueReleaseGLObjects(&images);
	queue.finish();
}
