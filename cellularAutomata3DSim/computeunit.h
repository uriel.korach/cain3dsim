#pragma once

#define CL_USE_DEPRECATED_OPENCL_1_2_APIS

#include <CL/cl.hpp>
#include <QOpenGLFunctions_4_5_Core>
#include <QOpenGLWidget>
#include <fstream>
#include <iostream>
#include <string>

class ComputeUnit
{
public:
	ComputeUnit();
	~ComputeUnit();

	void initializeCL();
	void compute(int width, int height, int depth);
	void createImageFromGL(cl_GLenum target, GLuint dataID1, GLuint dataID2);
	void releaseImages();
	void CreateProgram(const std::string & file, const std::string & function);
	void releaseImageAndAll();
	void switchData();
	int survive = 0x7ffe000;
	int born = 0xe6000;
	bool isMoore;
	float numOfStates;
	int first, second;
private:
	QOpenGLFunctions_4_5_Core *opengl;

	cl::Context context;
	cl::Platform platform;
	cl::Device device;
	cl::Program program;
	cl::Kernel kernel;
	cl::CommandQueue queue;
	cl::ImageGL *ImageData1, *ImageData2;
	
};

