#include "mainwindow.h"
#include <QDialog>
#include <QFormLayout>
#include <QLineEdit>
#include <QDialogButtonBox>
#include <QComboBox>


MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	timer = new QTimer(this);
	connect(ui.actionnew, SIGNAL(triggered()), this, SLOT(newSessionDialog()));
	connect(timer,        SIGNAL(timeout()),   this, SLOT(stepOneGeneration()));
	connect(ui.stepButton, SIGNAL(released()), this, SLOT(stepOneGeneration()));
	connect(ui.speed, SIGNAL(valueChanged(int)), this, SLOT(changeSpeed(int)));
	connect(ui.spacing, SIGNAL(valueChanged(int)), this, SLOT(changeSpacing(int)));
	connect(ui.playButton, SIGNAL(released()), this, SLOT(startClock()));
	connect(ui.stopButton, SIGNAL(released()), this, SLOT(stopClock()));
	connect(ui.cam, SIGNAL(stateChanged(int)), this, SLOT(changeCameraMode()));

	connect(ui.br0, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br1, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br2, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br3, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br4, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br5, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br6, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br7, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br8, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br9, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br10, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br11, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br12, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br13, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br14, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br15, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br16, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br17, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br18, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br19, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br20, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br21, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br22, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br23, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br24, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br25, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.br26, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr0, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr1, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr2, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr3, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr4, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr5, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr6, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr7, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr8, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr9, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr10, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr11, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr12, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr13, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr14, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr15, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr16, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr17, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr18, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr19, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr20, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr21, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr22, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr23, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr24, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr25, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
	connect(ui.sr26, SIGNAL(stateChanged(int)), this, SLOT(changeRule()));
}

void MainWindow::newSessionDialog()
{
	if (timer->isActive())
		timer->stop();

	QDialog dialog(this);
	dialog.setMinimumWidth(200);
	dialog.setMinimumHeight(100);
	dialog.setWindowTitle("New Session");
	QFormLayout form(&dialog);

	form.addRow(new QLabel("Fill Dimensions:"));

	QLineEdit widthEdit, heightEdit, depthEdit, states;
	widthEdit.setText("100");
	heightEdit.setText("100");
	depthEdit.setText("100");
	states.setText("2");
	form.addRow("Width:", &widthEdit);
	form.addRow("Height:", &heightEdit);
	form.addRow("Depth:", &depthEdit);
	form.addRow("Number of states:", &states);

	QComboBox combo1, combo2, randSize;
	combo1.addItems(QStringList() << "Random" << "Full" << "Single Cell");
	form.addRow("Choose Full/Random", &combo1);

	randSize.addItems(QStringList() << "Fill all" << "Fill only 10%");
	form.addRow("If Random, Choose Fill", &randSize);

	combo2.addItems(QStringList() << "Moore" << "Von  Neumann");
	form.addRow("Choose Neighborhood", &combo2);
	QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
		Qt::Horizontal, &dialog);
	form.addRow(&buttonBox);

	QObject::connect(&buttonBox, SIGNAL(accepted()), &dialog, SLOT(accept()));
	QObject::connect(&buttonBox, SIGNAL(rejected()), &dialog, SLOT(reject()));

	if (dialog.exec() == QDialog::Accepted) {
		int width = widthEdit.text().toInt();
		width = width > 1 ? (width < 201 ? width : 200) : 1;
		int height = heightEdit.text().toInt();
		height = height > 1 ? (height < 201 ? height : 200) : 1;
		int depth = depthEdit.text().toInt();
		depth = depth > 1 ? (depth < 201 ? depth : 200) : 1;
		int isFull = 0;
		if (combo1.currentText() == QString("Full"))
			isFull = 2;
		else if(combo1.currentText() == QString("Random"))
			isFull = 1;
		int numOfStates = states.text().toInt();
		float size;
		if (randSize.currentText() == QString("Fill all"))
			size = 0;
		else size = 0.45;
		ui.Display->createNewLattice(width, height, depth, isFull, numOfStates, size);

		bool isMoore = (combo2.currentText() == QString("Moore"));
		if (First)
		{
			CU.initializeCL();
			First = false;
		}
		else
			CU.releaseImages();
		CU.createImageFromGL(ui.Display->lattice->dataType, ui.Display->lattice->dataID1, ui.Display->lattice->dataID2);
		CU.first = 0;
		CU.second = 1;
		CU.isMoore = isMoore;
		CU.numOfStates = numOfStates;

		ui.cam->setCheckState(Qt::CheckState::Unchecked);
	}
}

void MainWindow::stepOneGeneration()
{
	
	if (!First)
	{
		ui.Display->lattice->Odd = !(ui.Display->lattice->Odd);
		CU.compute(ui.Display->lattice->w, ui.Display->lattice->h, ui.Display->lattice->d);
		CU.switchData();
		ui.Display->update();
	}
	
}


void MainWindow::changeRule()
{
	int born = 0;
	if (ui.br26->isChecked()) ++born;
	born <<= 1;
	if (ui.br25->isChecked()) ++born;
	born <<= 1;
	if (ui.br24->isChecked()) ++born;
	born <<= 1;
	if (ui.br23->isChecked()) ++born;
	born <<= 1;
	if (ui.br22->isChecked()) ++born;
	born <<= 1;
	if (ui.br21->isChecked()) ++born;
	born <<= 1;
	if (ui.br20->isChecked()) ++born;
	born <<= 1;
	if (ui.br19->isChecked()) ++born;
	born <<= 1;
	if (ui.br18->isChecked()) ++born;
	born <<= 1;
	if (ui.br17->isChecked()) ++born;
	born <<= 1;
	if (ui.br16->isChecked()) ++born;
	born <<= 1;
	if (ui.br15->isChecked()) ++born;
	born <<= 1;
	if (ui.br14->isChecked()) ++born;
	born <<= 1;
	if (ui.br13->isChecked()) ++born;
	born <<= 1;
	if (ui.br12->isChecked()) ++born; 
	born <<= 1;
	if (ui.br11->isChecked()) ++born;
	born <<= 1;
	if (ui.br10->isChecked()) ++born;
	born <<= 1;
	if (ui.br9->isChecked()) ++born;
	born <<= 1;
	if (ui.br8->isChecked()) ++born;
	born <<= 1;
	if (ui.br7->isChecked()) ++born;
	born <<= 1;
	if (ui.br6->isChecked()) ++born;
	born <<= 1;
	if (ui.br5->isChecked()) ++born;
	born <<= 1;
	if (ui.br4->isChecked()) ++born;
	born <<= 1;
	if (ui.br3->isChecked()) ++born;
	born <<= 1;
	if (ui.br2->isChecked()) ++born;
	born <<= 1;
	if (ui.br1->isChecked()) ++born;
	born <<= 1;
	if (ui.br0->isChecked()) ++born;

	int survive = 0;
	if (ui.sr26->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr25->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr24->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr23->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr22->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr21->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr20->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr19->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr18->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr17->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr16->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr15->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr14->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr13->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr12->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr11->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr10->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr9->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr8->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr7->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr6->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr5->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr4->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr3->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr2->isChecked()) ++survive;
	survive <<= 1;
	if (ui.sr1->isChecked()) ++survive;
	survive <<= 1; 
	if (ui.sr0->isChecked()) ++survive;

	CU.born = born;
	CU.survive = survive;
}


void MainWindow::startClock()
{
	timer->start(speed);
}

void MainWindow::stopClock()
{
	timer->stop();
}

void MainWindow::changeSpeed(int s)
{
	speed = 1000 / s;
	if (timer->isActive())
	{
		timer->stop();
		timer->start(speed);
	}
}

void MainWindow::changeSpacing(int s)
{
	ui.Display->cam.spacing = ui.Display->spacing = s*0.1 + 1;

	ui.Display->cam.rotate(0, 0);
	ui.Display->update();
}

void MainWindow::changeCameraMode()
{
	if (!ui.cam->isChecked()) {
		ui.Display->cam = Camera(ui.Display->cam.initPos, ui.Display->w, ui.Display->h, ui.Display->spacing);
		ui.Display->update();
	}
	else
		ui.Display->cam.isFPSMode = true;
}
