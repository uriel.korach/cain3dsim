#pragma once

#include <QOpenGLWidget>
#include <QOpenGLFunctions_4_5_Core>
#include <QOpenGLShaderProgram>
#include <QMouseEvent>
#include <QKeyEvent>
#include "camera.h"
#include "lattice.h"

class DisplayWidget : public QOpenGLWidget
{
	Q_OBJECT

public:
	DisplayWidget(QWidget *parent);
	~DisplayWidget();
	void createNewLattice(int width, int height, int depth, int isFull, int numOfStates, float size);

	QOpenGLFunctions_4_5_Core opengl;
	QOpenGLShaderProgram program;
	Lattice *lattice;
	float spacing = 1.2f;
	Camera cam;
	float w = 800, h = 600;

protected:
	void initializeGL() override;
	void paintGL() override;
	void resizeGL(int w, int h) override;
	void mousePressEvent(QMouseEvent *event) override;
	void mouseMoveEvent(QMouseEvent *event) override;
	void wheelEvent(QWheelEvent *event) override;
	void keyPressEvent(QKeyEvent *event) override;
	void keyReleaseEvent(QKeyEvent *event) override;
	
private:
	
	QPoint lastPos;
	float speedCam = 0.2;
	float zoom = 45;
	

};

