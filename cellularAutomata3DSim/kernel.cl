
__constant sampler_t  Sampler = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_NONE | CLK_FILTER_NEAREST;

__kernel void basicCA(__read_only  image2d_array_t srcImg,
					  __write_only image2d_array_t dstImg,
					  int survive, int born, int isMoore, float states)
{
	int4 coord = (int4)(get_global_id(0), get_global_id(1), get_global_id(2), 100);
	int4 dim = (int4)(get_global_size(0), get_global_size(1), get_global_size(2), 0) - (int4)(1,1,1,1);
	int i, j, k;
	
	float4 color = (float4)read_imagef(srcImg, Sampler, coord);

	float countN = 0;
	if (isMoore == 1)
	{
		for (k = coord.z - 1; k <= coord.z + 1; ++k)
		{
			for (j = coord.y - 1; j <= coord.y + 1; ++j)
			{
				for (i = coord.x - 1; i <= coord.x + 1; ++i)
				{
					int x =i, y = j, z = k;
					
					if (i == dim.x) x = 0;
					else if(i == -1) x = dim.x - 1;
					
					if (j == dim.y) y = 0;
					else if(j == -1) y = dim.y - 1;
					
					if (k == dim.z) z = 0;
					else if(k == -1) z = dim.z - 1;
					
					int4 neighbor = (int4)(x, y, z, 0);
					
					if(neighbor.x != coord.x || neighbor.y != coord.y ||  neighbor.z != coord.z) {
						float4 colorN = (float4)read_imagef(srcImg, Sampler, neighbor);
				
						if (colorN.w >= states - 1.0f) countN = countN + 1.0f;
					}
				}
			}
		}

	}
	else
	{
		int4 neighbor = clamp(coord + (int4)(0, 0, 1, 0), (int4)(0,0,0,0), dim);
		float4 colorN = (float4)read_imagef(srcImg, Sampler, neighbor);
		countN = countN + colorN.w;
		neighbor = clamp(coord + (int4)(0, 0, -1, 0), (int4)(0,0,0,0), dim);
		colorN = (float4)read_imagef(srcImg, Sampler, neighbor);
		countN = countN + colorN.w;
		
		neighbor = clamp(coord + (int4)(0, 1, 0, 0), (int4)(0,0,0,0), dim);
		colorN = (float4)read_imagef(srcImg, Sampler, neighbor);
		countN = countN + colorN.w;
		neighbor = clamp(coord + (int4)(0, -1, 0, 0), (int4)(0,0,0,0), dim);
		colorN = (float4)read_imagef(srcImg, Sampler, neighbor);
		countN = countN + colorN.w;
		
		neighbor = clamp(coord + (int4)(1, 0, 0, 0), (int4)(0,0,0,0), dim);
		colorN = (float4)read_imagef(srcImg, Sampler, neighbor);
		countN = countN + colorN.w;
		neighbor = clamp(coord + (int4)(-1, 0, 0, 0), (int4)(0,0,0,0), dim);
		colorN = (float4)read_imagef(srcImg, Sampler, neighbor);
		countN = countN + colorN.w;
	}

	int x = survive >> (int)countN;
	int y = born >> (int)countN;
	if ((color.w >= states - 1.0f) && ((1 & x) == 1))
	{
		color.w = states - 1.0f;
		//color.x = color.x+get_global_id(0)*0.01f/(get_global_size(0));
		//color.y = color.y+get_global_id(1)*0.01f/(get_global_size(1));
		//color.z = color.z+get_global_id(2)*0.01f/(get_global_size(2));

	}
	else if ((color.w < 0.5f) && ((1 & y) == 1))
	{
		color.w = states - 1.0f;
		//color.x = get_global_id(0)*0.5f/(get_global_size(0));
		//color.y = get_global_id(1)*0.5f/(get_global_size(1));
		//color.z = get_global_id(2)*0.5f/(get_global_size(2));

	}	
	else{
		color.w = color.w - 1.0f;
	}
	if(color.w < -0.5f) color.w = 0.0f;
	write_imagef(dstImg, coord, color);

	
} 