#include "displaywidget.h"
#include <QFile>
#include <QMessageBox>

DisplayWidget::DisplayWidget(QWidget *parent)
	: QOpenGLWidget(parent), lattice(nullptr), cam(QVector3D(10, 10, 10), 1, 1, spacing)
{
	setFocusPolicy(Qt::StrongFocus);
}

DisplayWidget::~DisplayWidget()
{
	if (lattice)
	{
		lattice->deleteData();
		lattice->deleteLattice();
		delete lattice;
	}
}

void DisplayWidget::initializeGL()
{
	opengl.initializeOpenGLFunctions();
	opengl.glClearColor(0, 0, 0.08, 1);
	opengl.glEnable(GL_BLEND);
	opengl.glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	QFile vertextShaderFile("vertexShader.gl"), fragmentShaderFile("fragmentShader.gl");
	vertextShaderFile.open(QIODevice::ReadOnly);
	fragmentShaderFile.open(QIODevice::ReadOnly);
	QString vertexShaderSource(vertextShaderFile.readAll());
	QString fragmentShaderSource(fragmentShaderFile.readAll());
	program.addShaderFromSourceCode(QOpenGLShader::Vertex, vertexShaderSource);
	program.addShaderFromSourceCode(QOpenGLShader::Fragment, fragmentShaderSource);
	program.link();
}

void DisplayWidget::paintGL()
{
	opengl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	opengl.glEnable(GL_DEPTH_TEST);
	opengl.glEnable(GL_CULL_FACE);
	program.bind();
	program.setUniformValue("cameraPosition", cam.position);
	program.setUniformValue("projViewMatrix", cam.perspective*cam.lookAt);
	program.setUniformValue("size", 1.0f);
	program.setUniformValue("distance", spacing);
	if(lattice)
		lattice->paint();
	program.release();
}

void DisplayWidget::resizeGL(int w_, int h_)
{
	cam.perspective.setToIdentity();
	cam.perspective.perspective(zoom, GLfloat(w_) / h_, 0.01f, 2000.0f);
	w = GLfloat(w_);
	h = GLfloat(h_);
}

void DisplayWidget::createNewLattice(int width, int height, int depth, int isFull, int numOfStates, float size)
{
	makeCurrent();
	if (lattice)
	{
		lattice->deleteData();
		lattice->deleteLattice();
		delete lattice;
	}
	
	lattice = new Lattice(&opengl, width, height, depth);
	lattice->createLattice();
	lattice->createData(GL_TEXTURE_2D_ARRAY, isFull, numOfStates, size);
	cam = Camera(QVector3D(width * 2, height * 2, depth * 2), w, h, spacing);
	update();
}

void DisplayWidget::mousePressEvent(QMouseEvent * event)
{
	lastPos = event->pos();
}

void DisplayWidget::mouseMoveEvent(QMouseEvent * event)
{
	int dx = event->x() - lastPos.x();
	int dy = -event->y() + lastPos.y();

	if (event->buttons() & Qt::LeftButton) {
		lastPos = event->pos();
		cam.rotate(dy, -dx);
		update();
	}


}

void DisplayWidget::wheelEvent(QWheelEvent * event)
{
	zoom += event->angleDelta().y()/80;
	zoom = zoom < 10 ? 10 : zoom;
	zoom = zoom > 90 ? 90 : zoom;
	cam.perspective.setToIdentity();
	cam.perspective.perspective(zoom, w / h, 0.01f, 2000.0f);
	update();
}

void DisplayWidget::keyPressEvent(QKeyEvent *event)
{
	speedCam = speedCam > 20 ? 20 : speedCam * 1.1;
	if (event->key() == Qt::Key_Right)
	{
		cam.move(RIGHT, speedCam);
	}
	if (event->key() == Qt::Key_Left)
	{
		cam.move(LEFT, speedCam);
	}
	if (event->key() == Qt::Key_Up)
	{
		cam.move(UP, speedCam);
	}
	if (event->key() == Qt::Key_Down)
	{
		cam.move(DOWN, speedCam);
	}

	update();

}

void DisplayWidget::keyReleaseEvent(QKeyEvent *event)
{
	if (event->isAutoRepeat())
		return;
	speedCam = 0.2;
}

