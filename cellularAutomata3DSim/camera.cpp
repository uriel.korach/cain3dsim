#include "camera.h"


#define MAX(x,y,z) ((x) > (y) ? ((x) > (z) ? (x) : (z)) : (y))

Camera::Camera(QVector3D _initPos, float w, float h, float spacing_)
{
	spacing = spacing_;
	direction = QVector3D(-1, -1, -1);
	direction.normalize();
	float maxDim = MAX(_initPos.x(), _initPos.y(), _initPos.z()); 
	initPos = QVector3D(maxDim, maxDim, maxDim);
	position = spacing * initPos;
	up = QVector3D(0, 1, 0);
	right = QVector3D::crossProduct(direction, up);
	right.normalize();
	rotation.setToIdentity();
	translation.setToIdentity();
	lookAt.setToIdentity();
	lookAt.lookAt(position, 0.25*position, up);
	perspective.setToIdentity();
	perspective.perspective(45, w/h, 0.01f, 2000.0f);
}


Camera::~Camera()
{
}

void Camera::rotate(int pitch, int yaw)
{

	rotation.setToIdentity();
	rotation.rotate(pitch*0.5, right);
	rotation.rotate(yaw*0.5, up);
	direction.normalize();
	QVector3D directionTemp = rotation * direction;
	directionTemp.normalize();
	float dotProd = QVector3D::dotProduct(directionTemp, up);
	if (dotProd < 0.95 && dotProd > -0.95)
	{
		if (isFPSMode)
		{
			direction = directionTemp;
			right = QVector3D::crossProduct(direction, up);
			right.normalize();
			lookAt.setToIdentity();
			lookAt.lookAt(position, position + direction, up);
		}
		else
		{
			direction = directionTemp;
			position = spacing *0.25 * initPos + 0.75 * (spacing*initPos).length() * (-1 * direction);
			right = QVector3D::crossProduct(direction, up);
			right.normalize();
			lookAt.setToIdentity();
			lookAt.lookAt(position, 0.25 * spacing*initPos, up);
		}
	}
}

void Camera::move(Direction dir, float speed)
{
	if (isFPSMode)
	{
		switch (dir) {
		default:
			break;
		case UP:
			position += direction * speed;
			break;
		case DOWN:
			position += -direction * speed;
			break;
		case RIGHT:
			position += right * speed;
			break;
		case LEFT:
			position += -right * speed;
			break;
		}

		lookAt.setToIdentity();
		lookAt.lookAt(position, position + direction, up);
	}
}


