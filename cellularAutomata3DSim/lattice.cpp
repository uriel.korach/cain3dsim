#include "lattice.h"
#include <vector>

static QRandomGenerator Rand(1000000);

Lattice::Lattice(QOpenGLFunctions_4_5_Core *ogl, unsigned width, unsigned height, unsigned depth) :
	opengl(ogl), w(width), h(height), d(depth), VAO(0), VBO(0), TCO(0), EBO(0), dataID1(0), dataID2(0),
	isSet(false)
{
}


Lattice::~Lattice()
{
}

/*
 *Creates 2 opengl texture from the type specified and in the dimensions of the lattice.
 *Loads it to the graphic card and keeps the IDs
 *
 */
void Lattice::createData(GLenum dataType_, int isFull, int numOfStates, float size)
{
	
	dataType = dataType_;


	
	
	float *data = new float[w*h*d*4];
	for (unsigned k = 0; k < d; ++k)
	{
		for (unsigned j = 0; j < h; ++j)
		{
			for (unsigned i = 0; i < w; ++i)
			{

				data[0 + (i + j * w + k * w * h) * 4] = i*0.5 / w;
				data[1 + (i + j * w + k * w * h) * 4] = j*0.5 / h;
				data[2 + (i + j * w + k * w * h) * 4] = k*0.5 / d;
				if (isFull == 2)
				{
					data[3 + (i + j * w + k * w * h) * 4] = numOfStates-1;
				}
				else if (isFull == 1)
				{
					bool incube = ((w-1)*(size) <= i && w*(1 - size) >= i);
					incube = incube && ((h-1)*(size) <= j && h*(1 - size) >= j);
					incube = incube && ((d-1)*(size) <= k && d*(1 - size) >= k);
					if ((Rand.generate() % 7797 > 3898) && incube)
						data[3 + (i + j * w + k * w * h) * 4] = numOfStates - 1;
					else
						data[3 + (i + j * w + k * w * h) * 4] = 0;
				}
				else
				{
					if (i == w * 0.5 && j == h * 0.5 && k == d * 0.5)
						data[3 + (i + j * w + k * w * h) * 4] = numOfStates-1;
					else
						data[3 + (i + j * w + k * w * h) * 4] = 0;
				}
				
			}
		}
	}
	opengl->glGenTextures(1, &dataID1);
	opengl->glBindTexture(dataType, dataID1);

	opengl->glTexParameteri(dataType, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	opengl->glTexParameteri(dataType, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	opengl->glTexParameteri(dataType, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	opengl->glTexParameteri(dataType, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	opengl->glTexParameteri(dataType, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	opengl->glTexStorage3D(dataType, 1, GL_RGBA32F, w, h, d);
	opengl->glTexSubImage3D(dataType, 0, 0, 0, 0, w, h, d, GL_RGBA, GL_FLOAT, data);
	opengl->glFinish();
	opengl->glGenTextures(1, &dataID2);
	opengl->glBindTexture(dataType, dataID2);

	opengl->glTexParameteri(dataType, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	opengl->glTexParameteri(dataType, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	opengl->glTexParameteri(dataType, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	opengl->glTexParameteri(dataType, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	opengl->glTexParameteri(dataType, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	opengl->glTexStorage3D(dataType, 1, GL_RGBA32F, w, h, d);
	opengl->glTexSubImage3D(dataType, 0, 0, 0, 0, w, h, d, GL_RGBA, GL_FLOAT, data);
	
	opengl->glBindTexture(dataType, 0);
	data1XorData2 = dataID1 ^ dataID2;
	delete[] data;
}

void Lattice::deleteData()
{
	opengl->glDeleteTextures(1, &dataID1);
	opengl->glDeleteTextures(1, &dataID2);
	opengl->glFinish();
	dataID1 = 0;
	dataID2 = 0;
}

void Lattice::createLattice()
{
	std::vector<unsigned char> vertices;
	std::vector<point> texcoords;
	std::vector<square> indices;
	for (unsigned char k = 0; k < d; ++k)
	{
		for (unsigned char j = 0; j < h; ++j)
		{
			for (unsigned char i = 0; i < w; ++i)
			{
				vertices.push_back(7);
				vertices.push_back(5);
				vertices.push_back(1);
				vertices.push_back(3);
				vertices.push_back(6);
				vertices.push_back(2);
				vertices.push_back(4);
				vertices.push_back(0);
				point v;
				v = { i, j, k };
				texcoords.push_back(v);
				texcoords.push_back(v);
				texcoords.push_back(v);
				texcoords.push_back(v);
				texcoords.push_back(v);
				texcoords.push_back(v);
				texcoords.push_back(v);
				texcoords.push_back(v);


				unsigned  offset = (i + j * w + k * w*h) * 8;
				square s = { 0 + offset, 3 + offset, 2 + offset, 0 + offset, 2 + offset, 1 + offset }; //front
				indices.push_back(s);
				s = { 0 + offset, 4 + offset, 5 + offset, 0 + offset, 5 + offset, 3 + offset }; //up
				indices.push_back(s);
				s = { 0 + offset, 1 + offset, 6 + offset, 0 + offset, 6 + offset, 4 + offset }; //right
				indices.push_back(s);
				s = { 2 + offset, 3 + offset, 5 + offset, 2 + offset, 5 + offset, 7 + offset }; //back
				indices.push_back(s);
				s = { 1 + offset, 2 + offset, 7 + offset, 1 + offset, 7 + offset, 6 + offset }; //bottom
				indices.push_back(s);
				s = { 4 + offset, 6 + offset, 7 + offset, 4 + offset, 7 + offset, 5 + offset }; //left 
				indices.push_back(s);
			}
		}
	}

	opengl->glGenVertexArrays(1, &VAO);
	opengl->glGenBuffers(1, &TCO);
	opengl->glGenBuffers(1, &VBO);
	opengl->glGenBuffers(1, &EBO);

	opengl->glBindVertexArray(VAO);

	opengl->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	opengl->glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(Lattice::square), (GLvoid*)(&*(indices.begin())), GL_STATIC_DRAW);

	opengl->glBindBuffer(GL_ARRAY_BUFFER, TCO);
	opengl->glBufferData(GL_ARRAY_BUFFER, texcoords.size() * sizeof(Lattice::point), (GLvoid*)(&*(texcoords.begin())), GL_STATIC_DRAW);

	opengl->glVertexAttribPointer(1, 3, GL_UNSIGNED_BYTE, GL_FALSE, 3 * sizeof(GLubyte), (void*)0);

	opengl->glBindBuffer(GL_ARRAY_BUFFER, VBO);
	opengl->glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(unsigned char), (GLvoid*)(&*(vertices.begin())), GL_STATIC_DRAW);

	opengl->glVertexAttribPointer(0, 1, GL_UNSIGNED_BYTE, GL_FALSE, 1 * sizeof(GLubyte), (void*)0);

	
	opengl->glEnableVertexAttribArray(0);
	opengl->glEnableVertexAttribArray(1);
	opengl->glBindVertexArray(0);
}

void Lattice::deleteLattice()
{
	opengl->glBindVertexArray(VAO);
	opengl->glInvalidateBufferData(VBO);
	opengl->glInvalidateBufferData(EBO);
	opengl->glInvalidateBufferData(TCO);
	opengl->glDeleteBuffers(1, &VBO);
	opengl->glDeleteBuffers(1, &EBO);
	opengl->glDeleteBuffers(1, &TCO);
	opengl->glDeleteVertexArrays(1, &VAO);
	opengl->glFinish();
	VAO = 0;
	VBO = 0;
	EBO = 0;
	TCO = 0;
}

void Lattice::paint()
{
	opengl->glBindVertexArray(VAO);
	opengl->glBindTexture(dataType, Odd ? dataID2 : dataID1);
	opengl->glDrawElements(GL_TRIANGLES, w * h * d * 36, GL_UNSIGNED_INT, (void*)0);
	opengl->glBindVertexArray(0);
	opengl->glBindTexture(dataType, 0);
	opengl->glFinish();
}
