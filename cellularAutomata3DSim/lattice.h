#pragma once

#include <QOpenGLFunctions_4_5_Core>
#include <QRandomGenerator>


/*
 * Holds the inforamation of the lattice, gl ids and dimensions and also creates the cells for opengl.
 * Will create opengl buffers for the cells.
 */
class Lattice
{
public:
	Lattice(QOpenGLFunctions_4_5_Core *ogl, unsigned width, unsigned height, unsigned depth);
	~Lattice();
	void createData(GLenum dataType_, int isFull, int numOfStates, float size);
	void deleteData();
	void createLattice();
	void deleteLattice();
	void paint();

	QOpenGLFunctions_4_5_Core *opengl;
	unsigned w, h, d;
	GLuint VAO, VBO, TCO, EBO;
	GLuint dataID1, dataID2, data1XorData2;
	GLenum dataType;
	bool isSet;
	bool Odd = false;
	
	struct point
	{
		unsigned char x, y, z;
	};
	
	struct square
	{
		unsigned topRight1, topLeft, bottomLeft1, bottomLeft2, bottomRight, topRight2;
	};


};

