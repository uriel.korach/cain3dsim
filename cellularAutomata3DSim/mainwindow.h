#pragma once

#include <QtWidgets/QMainWindow>
#include <QTimer>
#include "ui_mainwindow.h"
#include "computeunit.h"

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = Q_NULLPTR);
	~MainWindow() {
		if (timer->isActive())
			timer->stop();
		delete timer;
	}
private:
	Ui::MainWindowClass ui;
	QTimer *timer;
	ComputeUnit CU;
	bool First = true;
	int speed = 1000;
private slots:
	void stepOneGeneration();
	void newSessionDialog();
	void changeRule();
	void startClock();
	void stopClock();
	void changeSpeed(int s);
	void changeSpacing(int s);
	void changeCameraMode();
};
