/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>
#include "displaywidget.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindowClass
{
public:
    QAction *actionthere;
    QAction *actionnew;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QCheckBox *br25;
    QCheckBox *br19;
    QCheckBox *sr20;
    QCheckBox *br2;
    QCheckBox *br14;
    QCheckBox *sr1;
    QCheckBox *sr2;
    QCheckBox *sr25;
    QCheckBox *sr7;
    QCheckBox *br13;
    QPushButton *playButton;
    QCheckBox *br1;
    QCheckBox *br7;
    QCheckBox *sr26;
    QCheckBox *br0;
    QLabel *label_9;
    QLabel *label_8;
    DisplayWidget *Display;
    QLabel *label_4;
    QLabel *label;
    QCheckBox *br8;
    QPushButton *stepButton;
    QFrame *line_2;
    QPushButton *stopButton;
    QLabel *label_7;
    QCheckBox *sr12;
    QCheckBox *sr24;
    QCheckBox *sr19;
    QFrame *line;
    QCheckBox *sr6;
    QCheckBox *sr11;
    QCheckBox *sr9;
    QSlider *speed;
    QCheckBox *sr18;
    QCheckBox *sr0;
    QCheckBox *sr3;
    QCheckBox *sr5;
    QCheckBox *sr10;
    QCheckBox *sr16;
    QCheckBox *br3;
    QSlider *spacing;
    QFrame *line_3;
    QLabel *label_6;
    QCheckBox *br26;
    QCheckBox *sr13;
    QCheckBox *sr4;
    QCheckBox *sr14;
    QCheckBox *sr21;
    QCheckBox *sr15;
    QSpacerItem *verticalSpacer;
    QCheckBox *sr17;
    QCheckBox *sr22;
    QCheckBox *sr8;
    QLabel *label_3;
    QCheckBox *br20;
    QLabel *label_2;
    QCheckBox *sr23;
    QCheckBox *br21;
    QCheckBox *br6;
    QCheckBox *br9;
    QCheckBox *br18;
    QCheckBox *br11;
    QCheckBox *br12;
    QCheckBox *br5;
    QCheckBox *br22;
    QCheckBox *br17;
    QCheckBox *br10;
    QCheckBox *br15;
    QCheckBox *br24;
    QCheckBox *br16;
    QCheckBox *br4;
    QCheckBox *br23;
    QCheckBox *cam;
    QMenuBar *menuBar;
    QMenu *menu;

    void setupUi(QMainWindow *MainWindowClass)
    {
        if (MainWindowClass->objectName().isEmpty())
            MainWindowClass->setObjectName(QStringLiteral("MainWindowClass"));
        MainWindowClass->resize(1031, 966);
        actionthere = new QAction(MainWindowClass);
        actionthere->setObjectName(QStringLiteral("actionthere"));
        actionnew = new QAction(MainWindowClass);
        actionnew->setObjectName(QStringLiteral("actionnew"));
        centralWidget = new QWidget(MainWindowClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        br25 = new QCheckBox(centralWidget);
        br25->setObjectName(QStringLiteral("br25"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(br25->sizePolicy().hasHeightForWidth());
        br25->setSizePolicy(sizePolicy);
        br25->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(br25, 18, 1, 1, 1);

        br19 = new QCheckBox(centralWidget);
        br19->setObjectName(QStringLiteral("br19"));
        sizePolicy.setHeightForWidth(br19->sizePolicy().hasHeightForWidth());
        br19->setSizePolicy(sizePolicy);
        br19->setMaximumSize(QSize(32, 20));
        br19->setChecked(true);

        gridLayout->addWidget(br19, 15, 1, 1, 1);

        sr20 = new QCheckBox(centralWidget);
        sr20->setObjectName(QStringLiteral("sr20"));
        sizePolicy.setHeightForWidth(sr20->sizePolicy().hasHeightForWidth());
        sr20->setSizePolicy(sizePolicy);
        sr20->setMaximumSize(QSize(32, 20));
        sr20->setChecked(true);

        gridLayout->addWidget(sr20, 33, 2, 1, 1);

        br2 = new QCheckBox(centralWidget);
        br2->setObjectName(QStringLiteral("br2"));
        sizePolicy.setHeightForWidth(br2->sizePolicy().hasHeightForWidth());
        br2->setSizePolicy(sizePolicy);
        br2->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(br2, 6, 2, 1, 1);

        br14 = new QCheckBox(centralWidget);
        br14->setObjectName(QStringLiteral("br14"));
        sizePolicy.setHeightForWidth(br14->sizePolicy().hasHeightForWidth());
        br14->setSizePolicy(sizePolicy);
        br14->setMaximumSize(QSize(32, 20));
        br14->setChecked(true);

        gridLayout->addWidget(br14, 12, 2, 1, 1);

        sr1 = new QCheckBox(centralWidget);
        sr1->setObjectName(QStringLiteral("sr1"));
        sizePolicy.setHeightForWidth(sr1->sizePolicy().hasHeightForWidth());
        sr1->setSizePolicy(sizePolicy);
        sr1->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(sr1, 23, 1, 1, 1);

        sr2 = new QCheckBox(centralWidget);
        sr2->setObjectName(QStringLiteral("sr2"));
        sizePolicy.setHeightForWidth(sr2->sizePolicy().hasHeightForWidth());
        sr2->setSizePolicy(sizePolicy);
        sr2->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(sr2, 23, 2, 1, 1);

        sr25 = new QCheckBox(centralWidget);
        sr25->setObjectName(QStringLiteral("sr25"));
        sizePolicy.setHeightForWidth(sr25->sizePolicy().hasHeightForWidth());
        sr25->setSizePolicy(sizePolicy);
        sr25->setMaximumSize(QSize(32, 20));
        sr25->setChecked(true);

        gridLayout->addWidget(sr25, 36, 1, 1, 1);

        sr7 = new QCheckBox(centralWidget);
        sr7->setObjectName(QStringLiteral("sr7"));
        sizePolicy.setHeightForWidth(sr7->sizePolicy().hasHeightForWidth());
        sr7->setSizePolicy(sizePolicy);
        sr7->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(sr7, 27, 1, 1, 1);

        br13 = new QCheckBox(centralWidget);
        br13->setObjectName(QStringLiteral("br13"));
        sizePolicy.setHeightForWidth(br13->sizePolicy().hasHeightForWidth());
        br13->setSizePolicy(sizePolicy);
        br13->setMaximumSize(QSize(32, 20));
        br13->setChecked(true);

        gridLayout->addWidget(br13, 12, 1, 1, 1);

        playButton = new QPushButton(centralWidget);
        playButton->setObjectName(QStringLiteral("playButton"));
        sizePolicy.setHeightForWidth(playButton->sizePolicy().hasHeightForWidth());
        playButton->setSizePolicy(sizePolicy);
        playButton->setMaximumSize(QSize(100, 60));

        gridLayout->addWidget(playButton, 1, 1, 1, 3);

        br1 = new QCheckBox(centralWidget);
        br1->setObjectName(QStringLiteral("br1"));
        sizePolicy.setHeightForWidth(br1->sizePolicy().hasHeightForWidth());
        br1->setSizePolicy(sizePolicy);
        br1->setMaximumSize(QSize(32, 20));
        br1->setChecked(false);

        gridLayout->addWidget(br1, 6, 1, 1, 1);

        br7 = new QCheckBox(centralWidget);
        br7->setObjectName(QStringLiteral("br7"));
        sizePolicy.setHeightForWidth(br7->sizePolicy().hasHeightForWidth());
        br7->setSizePolicy(sizePolicy);
        br7->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(br7, 9, 1, 1, 1);

        sr26 = new QCheckBox(centralWidget);
        sr26->setObjectName(QStringLiteral("sr26"));
        sizePolicy.setHeightForWidth(sr26->sizePolicy().hasHeightForWidth());
        sr26->setSizePolicy(sizePolicy);
        sr26->setMaximumSize(QSize(32, 20));
        sr26->setChecked(true);

        gridLayout->addWidget(sr26, 36, 2, 1, 1);

        br0 = new QCheckBox(centralWidget);
        br0->setObjectName(QStringLiteral("br0"));
        br0->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(br0, 5, 3, 1, 1);

        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setAlignment(Qt::AlignRight|Qt::AlignTop|Qt::AlignTrailing);

        gridLayout->addWidget(label_9, 45, 3, 1, 1);

        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout->addWidget(label_8, 45, 1, 1, 1);

        Display = new DisplayWidget(centralWidget);
        Display->setObjectName(QStringLiteral("Display"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(Display->sizePolicy().hasHeightForWidth());
        Display->setSizePolicy(sizePolicy1);
        Display->setMinimumSize(QSize(800, 600));

        gridLayout->addWidget(Display, 0, 5, 48, 1);

        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout->addWidget(label_4, 43, 1, 1, 1);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 40, 1, 1, 1);

        br8 = new QCheckBox(centralWidget);
        br8->setObjectName(QStringLiteral("br8"));
        sizePolicy.setHeightForWidth(br8->sizePolicy().hasHeightForWidth());
        br8->setSizePolicy(sizePolicy);
        br8->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(br8, 9, 2, 1, 1);

        stepButton = new QPushButton(centralWidget);
        stepButton->setObjectName(QStringLiteral("stepButton"));
        stepButton->setMaximumSize(QSize(100, 60));

        gridLayout->addWidget(stepButton, 0, 1, 1, 3);

        line_2 = new QFrame(centralWidget);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line_2, 3, 1, 1, 3);

        stopButton = new QPushButton(centralWidget);
        stopButton->setObjectName(QStringLiteral("stopButton"));
        sizePolicy.setHeightForWidth(stopButton->sizePolicy().hasHeightForWidth());
        stopButton->setSizePolicy(sizePolicy);
        stopButton->setMaximumSize(QSize(100, 60));

        gridLayout->addWidget(stopButton, 2, 1, 1, 3);

        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_7, 42, 3, 1, 1);

        sr12 = new QCheckBox(centralWidget);
        sr12->setObjectName(QStringLiteral("sr12"));
        sizePolicy.setHeightForWidth(sr12->sizePolicy().hasHeightForWidth());
        sr12->setSizePolicy(sizePolicy);
        sr12->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(sr12, 28, 3, 1, 1);

        sr24 = new QCheckBox(centralWidget);
        sr24->setObjectName(QStringLiteral("sr24"));
        sizePolicy.setHeightForWidth(sr24->sizePolicy().hasHeightForWidth());
        sr24->setSizePolicy(sizePolicy);
        sr24->setMaximumSize(QSize(32, 20));
        sr24->setChecked(true);

        gridLayout->addWidget(sr24, 34, 3, 1, 1);

        sr19 = new QCheckBox(centralWidget);
        sr19->setObjectName(QStringLiteral("sr19"));
        sizePolicy.setHeightForWidth(sr19->sizePolicy().hasHeightForWidth());
        sr19->setSizePolicy(sizePolicy);
        sr19->setMaximumSize(QSize(32, 20));
        sr19->setChecked(true);

        gridLayout->addWidget(sr19, 33, 1, 1, 1);

        line = new QFrame(centralWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line, 19, 1, 1, 3);

        sr6 = new QCheckBox(centralWidget);
        sr6->setObjectName(QStringLiteral("sr6"));
        sizePolicy.setHeightForWidth(sr6->sizePolicy().hasHeightForWidth());
        sr6->setSizePolicy(sizePolicy);
        sr6->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(sr6, 25, 3, 1, 1);

        sr11 = new QCheckBox(centralWidget);
        sr11->setObjectName(QStringLiteral("sr11"));
        sizePolicy.setHeightForWidth(sr11->sizePolicy().hasHeightForWidth());
        sr11->setSizePolicy(sizePolicy);
        sr11->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(sr11, 28, 2, 1, 1);

        sr9 = new QCheckBox(centralWidget);
        sr9->setObjectName(QStringLiteral("sr9"));
        sizePolicy.setHeightForWidth(sr9->sizePolicy().hasHeightForWidth());
        sr9->setSizePolicy(sizePolicy);
        sr9->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(sr9, 27, 3, 1, 1);

        speed = new QSlider(centralWidget);
        speed->setObjectName(QStringLiteral("speed"));
        speed->setMinimum(1);
        speed->setMaximum(100);
        speed->setSingleStep(1);
        speed->setValue(1);
        speed->setOrientation(Qt::Horizontal);
        speed->setTickPosition(QSlider::TicksBelow);

        gridLayout->addWidget(speed, 41, 1, 1, 3);

        sr18 = new QCheckBox(centralWidget);
        sr18->setObjectName(QStringLiteral("sr18"));
        sizePolicy.setHeightForWidth(sr18->sizePolicy().hasHeightForWidth());
        sr18->setSizePolicy(sizePolicy);
        sr18->setMaximumSize(QSize(32, 20));
        sr18->setChecked(true);

        gridLayout->addWidget(sr18, 31, 3, 1, 1);

        sr0 = new QCheckBox(centralWidget);
        sr0->setObjectName(QStringLiteral("sr0"));
        sr0->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(sr0, 22, 3, 1, 1);

        sr3 = new QCheckBox(centralWidget);
        sr3->setObjectName(QStringLiteral("sr3"));
        sizePolicy.setHeightForWidth(sr3->sizePolicy().hasHeightForWidth());
        sr3->setSizePolicy(sizePolicy);
        sr3->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(sr3, 23, 3, 1, 1);

        sr5 = new QCheckBox(centralWidget);
        sr5->setObjectName(QStringLiteral("sr5"));
        sizePolicy.setHeightForWidth(sr5->sizePolicy().hasHeightForWidth());
        sr5->setSizePolicy(sizePolicy);
        sr5->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(sr5, 25, 2, 1, 1);

        sr10 = new QCheckBox(centralWidget);
        sr10->setObjectName(QStringLiteral("sr10"));
        sizePolicy.setHeightForWidth(sr10->sizePolicy().hasHeightForWidth());
        sr10->setSizePolicy(sizePolicy);
        sr10->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(sr10, 28, 1, 1, 1);

        sr16 = new QCheckBox(centralWidget);
        sr16->setObjectName(QStringLiteral("sr16"));
        sizePolicy.setHeightForWidth(sr16->sizePolicy().hasHeightForWidth());
        sr16->setSizePolicy(sizePolicy);
        sr16->setMaximumSize(QSize(32, 20));
        sr16->setChecked(true);

        gridLayout->addWidget(sr16, 31, 1, 1, 1);

        br3 = new QCheckBox(centralWidget);
        br3->setObjectName(QStringLiteral("br3"));
        sizePolicy.setHeightForWidth(br3->sizePolicy().hasHeightForWidth());
        br3->setSizePolicy(sizePolicy);
        br3->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(br3, 6, 3, 1, 1);

        spacing = new QSlider(centralWidget);
        spacing->setObjectName(QStringLiteral("spacing"));
        spacing->setMaximum(10);
        spacing->setSingleStep(1);
        spacing->setValue(2);
        spacing->setOrientation(Qt::Horizontal);
        spacing->setTickPosition(QSlider::TicksBelow);
        spacing->setTickInterval(0);

        gridLayout->addWidget(spacing, 44, 1, 1, 3);

        line_3 = new QFrame(centralWidget);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line_3, 38, 1, 1, 3);

        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout->addWidget(label_6, 42, 1, 1, 1);

        br26 = new QCheckBox(centralWidget);
        br26->setObjectName(QStringLiteral("br26"));
        sizePolicy.setHeightForWidth(br26->sizePolicy().hasHeightForWidth());
        br26->setSizePolicy(sizePolicy);
        br26->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(br26, 18, 2, 1, 1);

        sr13 = new QCheckBox(centralWidget);
        sr13->setObjectName(QStringLiteral("sr13"));
        sizePolicy.setHeightForWidth(sr13->sizePolicy().hasHeightForWidth());
        sr13->setSizePolicy(sizePolicy);
        sr13->setMaximumSize(QSize(32, 20));
        sr13->setChecked(true);

        gridLayout->addWidget(sr13, 30, 1, 1, 1);

        sr4 = new QCheckBox(centralWidget);
        sr4->setObjectName(QStringLiteral("sr4"));
        sizePolicy.setHeightForWidth(sr4->sizePolicy().hasHeightForWidth());
        sr4->setSizePolicy(sizePolicy);
        sr4->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(sr4, 25, 1, 1, 1);

        sr14 = new QCheckBox(centralWidget);
        sr14->setObjectName(QStringLiteral("sr14"));
        sizePolicy.setHeightForWidth(sr14->sizePolicy().hasHeightForWidth());
        sr14->setSizePolicy(sizePolicy);
        sr14->setMaximumSize(QSize(32, 20));
        sr14->setChecked(true);

        gridLayout->addWidget(sr14, 30, 2, 1, 1);

        sr21 = new QCheckBox(centralWidget);
        sr21->setObjectName(QStringLiteral("sr21"));
        sizePolicy.setHeightForWidth(sr21->sizePolicy().hasHeightForWidth());
        sr21->setSizePolicy(sizePolicy);
        sr21->setMaximumSize(QSize(32, 20));
        sr21->setChecked(true);

        gridLayout->addWidget(sr21, 33, 3, 1, 1);

        sr15 = new QCheckBox(centralWidget);
        sr15->setObjectName(QStringLiteral("sr15"));
        sizePolicy.setHeightForWidth(sr15->sizePolicy().hasHeightForWidth());
        sr15->setSizePolicy(sizePolicy);
        sr15->setMaximumSize(QSize(32, 20));
        sr15->setChecked(true);

        gridLayout->addWidget(sr15, 30, 3, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 47, 1, 1, 3);

        sr17 = new QCheckBox(centralWidget);
        sr17->setObjectName(QStringLiteral("sr17"));
        sizePolicy.setHeightForWidth(sr17->sizePolicy().hasHeightForWidth());
        sr17->setSizePolicy(sizePolicy);
        sr17->setMaximumSize(QSize(32, 20));
        sr17->setChecked(true);

        gridLayout->addWidget(sr17, 31, 2, 1, 1);

        sr22 = new QCheckBox(centralWidget);
        sr22->setObjectName(QStringLiteral("sr22"));
        sizePolicy.setHeightForWidth(sr22->sizePolicy().hasHeightForWidth());
        sr22->setSizePolicy(sizePolicy);
        sr22->setMaximumSize(QSize(32, 20));
        sr22->setChecked(true);

        gridLayout->addWidget(sr22, 34, 1, 1, 1);

        sr8 = new QCheckBox(centralWidget);
        sr8->setObjectName(QStringLiteral("sr8"));
        sizePolicy.setHeightForWidth(sr8->sizePolicy().hasHeightForWidth());
        sr8->setSizePolicy(sizePolicy);
        sr8->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(sr8, 27, 2, 1, 1);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout->addWidget(label_3, 22, 1, 1, 2);

        br20 = new QCheckBox(centralWidget);
        br20->setObjectName(QStringLiteral("br20"));
        sizePolicy.setHeightForWidth(br20->sizePolicy().hasHeightForWidth());
        br20->setSizePolicy(sizePolicy);
        br20->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(br20, 15, 2, 1, 1);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout->addWidget(label_2, 5, 1, 1, 2);

        sr23 = new QCheckBox(centralWidget);
        sr23->setObjectName(QStringLiteral("sr23"));
        sizePolicy.setHeightForWidth(sr23->sizePolicy().hasHeightForWidth());
        sr23->setSizePolicy(sizePolicy);
        sr23->setMaximumSize(QSize(32, 20));
        sr23->setChecked(true);

        gridLayout->addWidget(sr23, 34, 2, 1, 1);

        br21 = new QCheckBox(centralWidget);
        br21->setObjectName(QStringLiteral("br21"));
        sizePolicy.setHeightForWidth(br21->sizePolicy().hasHeightForWidth());
        br21->setSizePolicy(sizePolicy);
        br21->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(br21, 15, 3, 1, 1);

        br6 = new QCheckBox(centralWidget);
        br6->setObjectName(QStringLiteral("br6"));
        sizePolicy.setHeightForWidth(br6->sizePolicy().hasHeightForWidth());
        br6->setSizePolicy(sizePolicy);
        br6->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(br6, 7, 3, 1, 1);

        br9 = new QCheckBox(centralWidget);
        br9->setObjectName(QStringLiteral("br9"));
        sizePolicy.setHeightForWidth(br9->sizePolicy().hasHeightForWidth());
        br9->setSizePolicy(sizePolicy);
        br9->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(br9, 9, 3, 1, 1);

        br18 = new QCheckBox(centralWidget);
        br18->setObjectName(QStringLiteral("br18"));
        sizePolicy.setHeightForWidth(br18->sizePolicy().hasHeightForWidth());
        br18->setSizePolicy(sizePolicy);
        br18->setMaximumSize(QSize(32, 20));
        br18->setChecked(true);

        gridLayout->addWidget(br18, 13, 3, 1, 1);

        br11 = new QCheckBox(centralWidget);
        br11->setObjectName(QStringLiteral("br11"));
        sizePolicy.setHeightForWidth(br11->sizePolicy().hasHeightForWidth());
        br11->setSizePolicy(sizePolicy);
        br11->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(br11, 10, 2, 1, 1);

        br12 = new QCheckBox(centralWidget);
        br12->setObjectName(QStringLiteral("br12"));
        sizePolicy.setHeightForWidth(br12->sizePolicy().hasHeightForWidth());
        br12->setSizePolicy(sizePolicy);
        br12->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(br12, 10, 3, 1, 1);

        br5 = new QCheckBox(centralWidget);
        br5->setObjectName(QStringLiteral("br5"));
        sizePolicy.setHeightForWidth(br5->sizePolicy().hasHeightForWidth());
        br5->setSizePolicy(sizePolicy);
        br5->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(br5, 7, 2, 1, 1);

        br22 = new QCheckBox(centralWidget);
        br22->setObjectName(QStringLiteral("br22"));
        sizePolicy.setHeightForWidth(br22->sizePolicy().hasHeightForWidth());
        br22->setSizePolicy(sizePolicy);
        br22->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(br22, 16, 1, 1, 1);

        br17 = new QCheckBox(centralWidget);
        br17->setObjectName(QStringLiteral("br17"));
        sizePolicy.setHeightForWidth(br17->sizePolicy().hasHeightForWidth());
        br17->setSizePolicy(sizePolicy);
        br17->setMaximumSize(QSize(32, 20));
        br17->setChecked(true);

        gridLayout->addWidget(br17, 13, 2, 1, 1);

        br10 = new QCheckBox(centralWidget);
        br10->setObjectName(QStringLiteral("br10"));
        sizePolicy.setHeightForWidth(br10->sizePolicy().hasHeightForWidth());
        br10->setSizePolicy(sizePolicy);
        br10->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(br10, 10, 1, 1, 1);

        br15 = new QCheckBox(centralWidget);
        br15->setObjectName(QStringLiteral("br15"));
        sizePolicy.setHeightForWidth(br15->sizePolicy().hasHeightForWidth());
        br15->setSizePolicy(sizePolicy);
        br15->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(br15, 12, 3, 1, 1);

        br24 = new QCheckBox(centralWidget);
        br24->setObjectName(QStringLiteral("br24"));
        sizePolicy.setHeightForWidth(br24->sizePolicy().hasHeightForWidth());
        br24->setSizePolicy(sizePolicy);
        br24->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(br24, 16, 3, 1, 1);

        br16 = new QCheckBox(centralWidget);
        br16->setObjectName(QStringLiteral("br16"));
        sizePolicy.setHeightForWidth(br16->sizePolicy().hasHeightForWidth());
        br16->setSizePolicy(sizePolicy);
        br16->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(br16, 13, 1, 1, 1);

        br4 = new QCheckBox(centralWidget);
        br4->setObjectName(QStringLiteral("br4"));
        sizePolicy.setHeightForWidth(br4->sizePolicy().hasHeightForWidth());
        br4->setSizePolicy(sizePolicy);
        br4->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(br4, 7, 1, 1, 1);

        br23 = new QCheckBox(centralWidget);
        br23->setObjectName(QStringLiteral("br23"));
        sizePolicy.setHeightForWidth(br23->sizePolicy().hasHeightForWidth());
        br23->setSizePolicy(sizePolicy);
        br23->setMaximumSize(QSize(32, 20));

        gridLayout->addWidget(br23, 16, 2, 1, 1);

        cam = new QCheckBox(centralWidget);
        cam->setObjectName(QStringLiteral("cam"));

        gridLayout->addWidget(cam, 46, 1, 1, 3);

        MainWindowClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindowClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1031, 21));
        menu = new QMenu(menuBar);
        menu->setObjectName(QStringLiteral("menu"));
        MainWindowClass->setMenuBar(menuBar);

        menuBar->addAction(menu->menuAction());
        menu->addAction(actionnew);

        retranslateUi(MainWindowClass);

        QMetaObject::connectSlotsByName(MainWindowClass);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindowClass)
    {
        MainWindowClass->setWindowTitle(QApplication::translate("MainWindowClass", "MainWindow", nullptr));
        actionthere->setText(QApplication::translate("MainWindowClass", "there", nullptr));
        actionnew->setText(QApplication::translate("MainWindowClass", "New", nullptr));
        br25->setText(QApplication::translate("MainWindowClass", "25", nullptr));
        br19->setText(QApplication::translate("MainWindowClass", "19", nullptr));
        sr20->setText(QApplication::translate("MainWindowClass", "20", nullptr));
        br2->setText(QApplication::translate("MainWindowClass", "2", nullptr));
        br14->setText(QApplication::translate("MainWindowClass", "14", nullptr));
        sr1->setText(QApplication::translate("MainWindowClass", "1", nullptr));
        sr2->setText(QApplication::translate("MainWindowClass", "2", nullptr));
        sr25->setText(QApplication::translate("MainWindowClass", "25", nullptr));
        sr7->setText(QApplication::translate("MainWindowClass", "7", nullptr));
        br13->setText(QApplication::translate("MainWindowClass", "13", nullptr));
        playButton->setText(QApplication::translate("MainWindowClass", "Play", nullptr));
        br1->setText(QApplication::translate("MainWindowClass", "1", nullptr));
        br7->setText(QApplication::translate("MainWindowClass", "7", nullptr));
        sr26->setText(QApplication::translate("MainWindowClass", "26", nullptr));
        br0->setText(QApplication::translate("MainWindowClass", "0", nullptr));
        label_9->setText(QApplication::translate("MainWindowClass", "1", nullptr));
        label_8->setText(QApplication::translate("MainWindowClass", "0", nullptr));
        label_4->setText(QApplication::translate("MainWindowClass", "<html><head/><body><p><span style=\" font-weight:600;\">Spacing</span></p></body></html>", nullptr));
        label->setText(QApplication::translate("MainWindowClass", "<html><head/><body><p><span style=\" font-weight:600;\">Speed</span></p></body></html>", nullptr));
        br8->setText(QApplication::translate("MainWindowClass", "8", nullptr));
        stepButton->setText(QApplication::translate("MainWindowClass", "Step", nullptr));
        stopButton->setText(QApplication::translate("MainWindowClass", "Pause", nullptr));
        label_7->setText(QApplication::translate("MainWindowClass", "Max", nullptr));
        sr12->setText(QApplication::translate("MainWindowClass", "12", nullptr));
        sr24->setText(QApplication::translate("MainWindowClass", "24", nullptr));
        sr19->setText(QApplication::translate("MainWindowClass", "19", nullptr));
        sr6->setText(QApplication::translate("MainWindowClass", "6", nullptr));
        sr11->setText(QApplication::translate("MainWindowClass", "11", nullptr));
        sr9->setText(QApplication::translate("MainWindowClass", "9", nullptr));
        sr18->setText(QApplication::translate("MainWindowClass", "18", nullptr));
        sr0->setText(QApplication::translate("MainWindowClass", "0", nullptr));
        sr3->setText(QApplication::translate("MainWindowClass", "3", nullptr));
        sr5->setText(QApplication::translate("MainWindowClass", "5", nullptr));
        sr10->setText(QApplication::translate("MainWindowClass", "10", nullptr));
        sr16->setText(QApplication::translate("MainWindowClass", "16", nullptr));
        br3->setText(QApplication::translate("MainWindowClass", "3", nullptr));
        label_6->setText(QApplication::translate("MainWindowClass", "Min", nullptr));
        br26->setText(QApplication::translate("MainWindowClass", "26", nullptr));
        sr13->setText(QApplication::translate("MainWindowClass", "13", nullptr));
        sr4->setText(QApplication::translate("MainWindowClass", "4", nullptr));
        sr14->setText(QApplication::translate("MainWindowClass", "14", nullptr));
        sr21->setText(QApplication::translate("MainWindowClass", "21", nullptr));
        sr15->setText(QApplication::translate("MainWindowClass", "15", nullptr));
        sr17->setText(QApplication::translate("MainWindowClass", "17", nullptr));
        sr22->setText(QApplication::translate("MainWindowClass", "22", nullptr));
        sr8->setText(QApplication::translate("MainWindowClass", "8", nullptr));
        label_3->setText(QApplication::translate("MainWindowClass", "<html><head/><body><p><span style=\" font-size:9pt; font-weight:600;\">Survive Rule:</span></p></body></html>", nullptr));
        br20->setText(QApplication::translate("MainWindowClass", "20", nullptr));
        label_2->setText(QApplication::translate("MainWindowClass", "<html><head/><body><p><span style=\" font-size:9pt; font-weight:600;\">Born Rule:</span></p></body></html>", nullptr));
        sr23->setText(QApplication::translate("MainWindowClass", "23", nullptr));
        br21->setText(QApplication::translate("MainWindowClass", "21", nullptr));
        br6->setText(QApplication::translate("MainWindowClass", "6", nullptr));
        br9->setText(QApplication::translate("MainWindowClass", "9", nullptr));
        br18->setText(QApplication::translate("MainWindowClass", "18", nullptr));
        br11->setText(QApplication::translate("MainWindowClass", "11", nullptr));
        br12->setText(QApplication::translate("MainWindowClass", "12", nullptr));
        br5->setText(QApplication::translate("MainWindowClass", "5", nullptr));
        br22->setText(QApplication::translate("MainWindowClass", "22", nullptr));
        br17->setText(QApplication::translate("MainWindowClass", "17", nullptr));
        br10->setText(QApplication::translate("MainWindowClass", "10", nullptr));
        br15->setText(QApplication::translate("MainWindowClass", "15", nullptr));
        br24->setText(QApplication::translate("MainWindowClass", "24", nullptr));
        br16->setText(QApplication::translate("MainWindowClass", "16", nullptr));
        br4->setText(QApplication::translate("MainWindowClass", "4", nullptr));
        br23->setText(QApplication::translate("MainWindowClass", "23", nullptr));
        cam->setText(QApplication::translate("MainWindowClass", " FPS Camera", nullptr));
        menu->setTitle(QApplication::translate("MainWindowClass", "Menu", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindowClass: public Ui_MainWindowClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
